# RushHour

Jeu Rush Hour en JavaScript.

#  Executer test
    mocha test

# Cahier des charges

    Le jeu consiste à deplacer la voiture 'rouge' jusqu'à la sortie.

## `Arborescence`

    rushhour
    |   .gitignore
    |   README.md
    +---src
    |       Board.js
    |       Bus.js
    |       Car.js
    |       Enums.js
    |       Vehicle.js
    |
    \---test
            BoardTest.js
            BusTest.js
            CarTest.js


## Source 

### `Plateau de jeu`

- Initialisation du plateau de jeu
- Vérification des positions des véhicules.
- Placements des objects sur le plateau.

### `Vehicule`
    
- Voiture
    - Longueur de la voiture est toujours égale à 2.

- Bus
    - Longueur du bus est toujours égale à 3.

### `Enumérations`

    Quelques émumérations.

## Tests

    Code coverage : 100%
    
    
## Roadmap
- Finir : la fonction "déplacer"
- Faire des test de collisons dans cette dernière
- Condition de sortie du jeu (lorsque l'on a gagné)
- Compter le nombre de déplacement.

