const {Vehicle} = require('./Vehicle');
const DEFAULT_LENGTH = 2;

class Car extends Vehicle {
    /**
     * Car object
     * @param mode (VERTICAL || HORIZONTAL)
     * @param startCoordinates
     */
    constructor(mode, startCoordinates) {
        super(DEFAULT_LENGTH, mode, startCoordinates);
    }

}

module.exports = {Car};
