const {MODE, ERROR} = require('./Enums');

class Vehicle {
    constructor(length, mode, startCoordinates) {
        if (mode !== MODE.VERTICAL && mode !== MODE.HORIZONTAL && mode !== undefined) {
            throw new Error(ERROR.MODE);
        }
        this.length = length;
        this.mode = mode;
        this.startCoordinates = startCoordinates;
    }

}

module.exports = {Vehicle};
