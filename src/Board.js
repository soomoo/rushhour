const {MODE, TAKEN, FREE} = require('./Enums');

class Board {

    constructor() {
        this.boardGame = [];
        this.vehicles = [];
    }

    /**
     * Initializes an array with specific parameters
     * @param lengthX
     * @param lengthY
     * @returns {[]|*[]}
     */
    initBoardGame = (lengthX, lengthY) => {
        if (lengthX === undefined || lengthY === undefined) {
            return [];
        }

        this.boardGame = new Array(lengthY).fill(FREE);
        for(let i = 0; i < this.boardGame.length; i++) {
            this.boardGame[i] = new Array(lengthX).fill(FREE);
        }
        return this.boardGame;
    };

    /**
     * Verify if new coordinates of a vehicle are possible
     * @param {Array} coordinates the coordinates user wants to set to vehicle
     * @param {Vehicle} vehicle the vehicle user wants to move
     * @returns {Boolean} new coordinates valid or not
     */
    posIsValid(coordinates, vehicle){

        //x coordinates outside board only
        if(coordinates[0] < 0)
            return false;
        if(coordinates[0] >= this.boardGame[0].length)
            return false;
        
        //y coordinates outside board only
        if(coordinates[1] < 0)
            return false;
        if(coordinates[1] >= this.boardGame[1].length)
            return false;

        //horizontal mode and length taken as parameter with x axis, whole vehicle can't be outside
        if(vehicle.mode === MODE.HORIZONTAL && coordinates[0] + vehicle.length >= this.boardGame[0].length)
            return false;

        //vertical mode and length taken as parameter with y axis, whole vehicle can't be outside
        if(vehicle.mode === MODE.VERTICAL && coordinates[1] + vehicle.length >= this.boardGame[1].length)
            return false;
        
        //default value
        return true;
    }

    move(vehicle, coordinates){

        vehicle.startCoordinates[0] = coordinates[0];
        vehicle.startCoordinates[1] = coordinates[1];

        return vehicle.startCoordinates;
    }

    /**
     * Placing objects in boardGame.
     * @param vehicles
     */
    placeObjects = (vehicles) => {
        vehicles.forEach(element => {

            this.boardGame[element.startCoordinates[0]][element.startCoordinates[1]] = element;
            // Adds an element on the board if it is horizontal.
            if (element.mode === MODE.HORIZONTAL) {
                for(let i = 1; i < element.length; i++) {
                    this.boardGame[element.startCoordinates[0]][element.startCoordinates[1] + i] = TAKEN;
                }
            }
            // Adds an element on the board if it is vertical.
            if (element.mode === MODE.VERTICAL) {
                for(let i = 1; i < element.length; i++) {
                    this.boardGame[element.startCoordinates[0] + i][element.startCoordinates[1]] = TAKEN;
                }
            }
        });
    };
}

module.exports = {Board};
