const {Vehicle} = require('./Vehicle');
const DEFAULT_LENGTH = 3;

class Bus extends Vehicle {
    /**
     * Bus object
     * @param mode (VERTICAL || HORIZONTAL)
     * @param startCoordinates
     */
    constructor(mode, startCoordinates) {
        super(DEFAULT_LENGTH, mode, startCoordinates);
    }
}

module.exports = {Bus};
