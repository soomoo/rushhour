/**
 * MODE
 * @type {{VERTICAL: string, HORIZONTAL: string}}
 */
const MODE = {
  VERTICAL: 'VERTICAL',
  HORIZONTAL: 'HORIZONTAL'
};

/**
 * ERROR
 * @type {{MODE: Error}}
 */
const ERROR = {
  MODE: new Error('The select mode isn\'t available'),
};

const TAKEN = 'TAKEN';
const FREE = 'FREE';

module.exports = {
  MODE,
  ERROR,
  TAKEN,
  FREE
};
