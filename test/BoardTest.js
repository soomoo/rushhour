const test = require('unit.js');
const { Board } = require('../src/Board');
const { Vehicle } = require('../src/Vehicle');
const { Car } = require('../src/Car');
const { MODE, TAKEN, FREE } = require('../src/Enums');

const board = new Board();

const emptyArray = [];
const lengthX = 4;
const lengthY = 5;

const arrayOfFree = new Array(lengthX).fill("FREE");
const arrayCustomWithLength = new Array(lengthY).fill(arrayOfFree);

//vehicle
const vehicleLength = 1;
const vehicleCoordinate = [0, 0];
const vehicle = new Vehicle(vehicleLength, MODE.HORIZONTAL, vehicleCoordinate);

const carMode = MODE.HORIZONTAL;
const carCoordinate = [1, 1];
const car = new Car(carMode, carCoordinate);


const carOne  = new Car(MODE.HORIZONTAL, [0, 0]);
const carTwo  = new Car(MODE.HORIZONTAL, [1, 0]);
const carThree  = new Car(MODE.HORIZONTAL, [2, 0]);
const arrayOfCars = [carOne, carTwo, carThree];

const carOneVert  = new Car(MODE.VERTICAL, [0, 0]);
const carTwoVert  = new Car(MODE.VERTICAL, [0, 1]);
const carThreeVert  = new Car(MODE.VERTICAL, [0, 2]);
const arrayOfCarsVert = [carOneVert, carTwoVert, carThreeVert];

const arrayCustomStatic = new Array(lengthY);
const arrayCustomStaticVert = new Array(lengthY);

describe('Testing board game functions', () => {

    it('Should return empty array', () => {
        test
            .array(board.initBoardGame())
            .is(emptyArray)
    });

    it(`Should return an empty array with ${lengthX} as length X and ${lengthY} as length Y`, () => {
        test
            .array(board.initBoardGame(lengthX, lengthY))
            .is(arrayCustomWithLength)
    });

    describe('posIsValid function', () =>{

        it('Should return true', () => {

            test.bool(board.posIsValid(vehicleCoordinate, vehicle)).isTrue();
        });

        it('Should return false if x coordinate < 0', () => {

            let newCoordinates = [-1, 1];
            test.bool(board.posIsValid(newCoordinates, vehicle)).isFalse();
        });

        it('Should return false if x coordinate >= width board', () => {

            let newCoordinates = [lengthX, 1];
            test.bool(board.posIsValid(newCoordinates, vehicle)).isFalse();
        });

        it('Should return false if y coordinate < 0', () => {

            let newCoordinates = [1, -1];
            test.bool(board.posIsValid(newCoordinates, vehicle)).isFalse();
        });

        it('Should return false if y coordinate >= height board', () => {

            let newCoordinates = [1, lengthY];
            test.bool(board.posIsValid(newCoordinates, vehicle)).isFalse();
        });

        it('Should return false if length is outside with horizontal mode', () => {

            let newCoordinates = [lengthX - car.length, 1];
            test.bool(board.posIsValid(newCoordinates, car)).isFalse();
        });

        it('Should return false if length is outside with vertical mode', () => {

            let newCoordinates = [1, lengthY - car.length];
            car.mode = MODE.VERTICAL;
            test.bool(board.posIsValid(newCoordinates, car)).isFalse();
        });

    });

    describe('move function', () => {

        it('Should return coordinates of the vehicle', () => {

            test.array(board.move(vehicle, vehicleCoordinate)).is(vehicleCoordinate);
        });

        it('Should set coordinates of the vehicle with new coordinates', () => {

            let newCoordinates = [2, 2];
            board.move(vehicle, newCoordinates);
            test.array(vehicle.startCoordinates).is(newCoordinates);
        });
    });

    describe('Testing placeObjects function', () => {

        for(let i = 0; i < arrayCustomStatic.length; i++) {
            arrayCustomStatic[i] = [...arrayOfFree];
        }

        arrayCustomStatic[0][0] = carOne;
        arrayCustomStatic[0][1] = TAKEN;
        arrayCustomStatic[1][0] = carTwo;
        arrayCustomStatic[1][1] = TAKEN;
        arrayCustomStatic[2][0] = carThree;
        arrayCustomStatic[2][1] = TAKEN;

        it('Sould be insert as horizontal', () => {
            board.initBoardGame(lengthX,lengthY);
            board.placeObjects(arrayOfCars);
            test
                .array(board.boardGame)
                .is(arrayCustomStatic)
        });

        for(let i = 0; i < arrayCustomStaticVert.length; i++) {
            arrayCustomStaticVert[i] = [...arrayOfFree];
        }

        arrayCustomStaticVert[0][0] = carOneVert;
        arrayCustomStaticVert[1][0] = TAKEN;
        arrayCustomStaticVert[0][1] = carTwoVert;
        arrayCustomStaticVert[1][1] = TAKEN;
        arrayCustomStaticVert[0][2] = carThreeVert;
        arrayCustomStaticVert[1][2] = TAKEN;

        it('Sould be insert as vertical', () => {
            board.initBoardGame(lengthX,lengthY);
            board.placeObjects(arrayOfCarsVert);
            test
                .array(board.boardGame)
                .is(arrayCustomStaticVert)
        });

    });

});
