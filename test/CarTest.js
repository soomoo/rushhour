const test = require('unit.js');
const {Car} = require('../src/Car');
const {MODE, ERROR} = require('../src/Enums');

const DEFAULT_LENGTH_CAR = 2;

describe('Testing Car functions', () => {

    it('Should return a Car with a length of two', () => {
        test
            .number(new Car().length)
            .is(DEFAULT_LENGTH_CAR)
    });

    it('Sould return a Car with HORIZONTAL mode', () => {
        test
            .string(new Car(MODE.HORIZONTAL).mode)
            .is(MODE.HORIZONTAL)
    });

    it('Sould return a Car with VERTICAL mode', () => {
        test
            .string(new Car(MODE.VERTICAL).mode)
            .is(MODE.VERTICAL)
    });

    it('Sould throw an error when a wrong mode is given', () => {
        test
            .error(() => {new Car('WRONG MODE')})
            .is(ERROR.MODE)
    });

});
