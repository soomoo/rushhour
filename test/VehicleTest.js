const test = require('unit.js');
const { Vehicle } = require('../src/Vehicle');
const {MODE, ERROR} = require('../src/Enums');

//vehicle
const vehicleLength = 1;
const vehicleMode  = MODE.HORIZONTAL;
const vehicleCoordinate = [1, 1];
const vehicle = new Vehicle(vehicleLength, vehicleMode, vehicleCoordinate);

describe('Testing Vehicle functions', () => {

    describe('Constructor', () => {

        it('Should have a length of one', () => {

            test.number(vehicle.length).is(vehicleLength);
        });

        it('Should have HORIZONTAL as mode', () => {

            test.string(vehicle.mode).is(vehicleMode);
        });

        it('Should have [1, 1] as coordinate', () => {

            test.array(vehicleCoordinate).is(vehicleCoordinate);
        });
    });

});
