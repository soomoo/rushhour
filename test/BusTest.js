const test = require('unit.js');
const {Bus} = require('../src/Bus');
const {MODE, ERROR} = require('../src/Enums');

const DEFAULT_LENGTH_BUS = 3;

describe('Testing Bus functions', () => {

    it('Should return a Bus with a length of two', () => {
        test
            .number(new Bus().length)
            .is(DEFAULT_LENGTH_BUS)
    });

    it('Sould return a Bus with HORIZONTAL mode', () => {
        test
            .string(new Bus(MODE.HORIZONTAL).mode)
            .is(MODE.HORIZONTAL)
    });

    it('Sould return a Bus with VERTICAL mode', () => {
        test
            .string(new Bus(MODE.VERTICAL).mode)
            .is(MODE.VERTICAL)
    });

    it('Sould throw an error when a wrong mode is given', () => {
        test
            .error(() => {new Bus('WRONG MODE')})
            .is(ERROR.MODE)
    });


});
